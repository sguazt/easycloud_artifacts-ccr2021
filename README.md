# Reproducibility of experiments presented in the paper *"An educational toolkit for teaching cloud computing"*

This guide describes the steps required to reproduce the experimental evaluation presented in the paper:

> Cosimo Anglano, Massimo Canonico, and Marco Guazzone.
> *An educational toolkit for teaching cloud computing.*
> Submitted for publication to ACM SIGCOMM Computer Communication Review.

This paper describes the use of [EasyCloud](https://gitlab.di.unipmn.it/DCS/easycloud/) (a multi-cloud, open-source toolkit which features a unified and easy interface to interact with multiple cloud platforms at the same time) in an educational context.

In the rest of this guide, we will denote with the environment variable `EC_ARTIFACTS_HOME` the directory where you cloned this project.
This variable will be set during the installation of EasyCloud (see below).


## Contents of this repository

* `docs`: a folder containing documentation files that are part of this guide.

* `easycloud-v2.0.2.zip`: the source package of EasyCloud version 2.0.2, which is the version used in the experimental evaluation.

* `README.md`: this file

* `resources`: a folder containing various resources used to reproduce the experimental evaluation.

* `scripts`: a folder containing shell, Python and R scripts used to reproduce the experimental evaluation.


## Steps for reproducibility

1. [Install EasyCloud](docs/install.md)

2. [Run AWS experiments](docs/aws.md)

3. [Run Chameleon experiments](docs/chameleon.md)
