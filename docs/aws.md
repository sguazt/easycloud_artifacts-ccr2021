# AWS experiments

In this guide, we describe the [steps](#steps-for-reproducibility) required to reproduce the experiments on AWS.
Before performing any experiment, make sure to satisfy all the necessary [prerequisites](#prerequisites).


## Prerequisites

* **An AWS account.**
For these experiments, you need an AWS account; if you don't have one, you can create a free account with the [AWS Free Tier](https://aws.amazon.com/it/free/) plan (however, note that if you exceeds the free-tier limits, you will pay standard, pay-as-you-go rates).

* **An AWS EC2 key pair.**
In order to connect to EC2 instances, you need to create a key pair.
To this aim, see the [AWS EC2 documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html) related to this topic.

* **The AWS CLI version 2.**
To create EC2 instances, we provide a shell script that uses the AWS CLI version 2.
Therefore, you need to install AWS CLI version 2 and configure it properly, as described in the [AWS CLI project page](https://github.com/aws/aws-cli) or in the [AWS documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html).
We strongly suggest installing the AWS CLI version 2 so as you can automatically create the needed VM instance.
However, as an alternative to using our shell script, you can also create the needed VM instances manually by using the Web-based [AWS management console](https://aws.amazon.com/console).

* **The environment variable `EC_ARTIFACTS_HOME` is properly set.**
The environment variable `EC_ARTIFACTS_HOME` must contain the path to the directory where you cloned this project; this variable has been set during the [installation of EasyCloud](docs/install.md).


## Configuration settings

EasyCloud requires a configuration file specific to each cloud platform where to read information (such as the user credentials) needed to access and configure the services provided by a given cloud platform.
In the following, we report the configuration settings specific to AWS; settings highlighted in bold text are mandatory.

* **`ec2_access_key_id`**: the AWS access key ID associated with either your IAM user or with your temporary security credentials.
For example, `ec2_access_key_id = AKIAIOSFODNN7EXAMPLE`.

* **`ec2_secret_access_key`**: the AWS secret access key associated with either your IAM user or with your temporary security credentials.
For example, `ec2_secret_access_key = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY`.

* `ec2_session_token`: the session token associated with your temporary security credentials.
For example, `ec2_session_token = AQoEXAMPLEH4aoAH0gNCAPyJxz4BlCFFxWNE1OPTgk5TthT+FvwqnKwRcOIfrRh3c/LTo6UDdyJwOOvEVPvLXCrrrUtdnniCEXAMPLE/IvU1dYUg2RVAJBanLiHb4IgRmpRV3zrkuWJOgQs8IZZaIv2BXIa2R4OlgkBN9bkUDNCJiBeb/AXlzBBko7b15fjrBs2+cTQtpZ3CYWFXG8C5zqx37wnOE49mRl/+OtkIKGO7fAE`.

* **`ec2_default_region`**: the name of the default region to use when not specified.
For example, we used `ec2_default_region = us-east-1`.

For more information about AWS credentials, visit the following AWS documentation pages: [managing access keys for IAM users](https://docs.aws.amazon.com/en_us/IAM/latest/UserGuide/id_credentials_access-keys.html) and to [using temporary security credentials with AWS resources](//docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp.html).


## Steps for reproducibility

### Preliminary steps

The following steps are to be performed only the first time you access AWS with EasyCloud.

1. **Create an AWS account.**

   _NOTE: if you already have an AWS account, you can skip this step._

   Visit the [AWS home page](https://aws.amazon.com) and sign up for a new account.

2. **Create an EC2 key pair.**

   _NOTE: if you already have an EC2 key pair, you can use skip this step._

   Follow the instructions available in the [AWS EC2 documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html).

3. **Install and configure the AWS CLI version 2.**

    1. To install AWS CLI version 2, follow the instructions available in the related [AWS CLI documentation page](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).
    2. To configure AWS CLI version 2, follow the instructions available in the related [AWS CLI documentation page](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html).

4. **Create 45 EC2 instances.**
    In the following, `<key-name>` denotes the name of your EC2 key pair.

        cd $EC_ARTIFACTS_HOME

        source myenv/bin/activate

        ./scripts/aws_manage_instances.sh create 1 45 <key-name>

        deactivate

5. **Create and edit the EasyCloud configuration file.**

	Before accessing AWS with EasyCloud the first time, you need to create a configuration file containing the security credentials of your AWS account.
    To do so, run the following commands:

        cd $EC_ARTIFACTS_HOME

        cd easycloud/modules/aws_awssdk

        cp settings.cfg.template settings.cfg

        vim settings.cfg

    Now, edit the configuration file `settings.cfg` to provide the ettings associated with your AWS account (e.g., `ec2_access_key_id` and `ec2_secret_access_key`).
    See [this section][#configuration-settings] for the list of configuration settings specific to AWS.


### Running experiments

1. **Run the experiments.**

        cd $EC_ARTIFACTS_HOME

        source myenv/bin/activate

        ./scripts/aws_easycloud.sh

        deactivate

   At the end of the execution, the `$EC_ARTIFACTS_HOME/logs/aws_easycloud` directory will contain the raw results.

2. **Analyze and plot results.**

        cd $EC_ARTIFACTS_HOME

        ./scripts/analyze_results.sh aws

   At the end of the execution, the `$EC_ARTIFACTS_HOME` directory will contain the following new files:
   * `aws.csv`: the raw results collected from the various log files generated by the experiments.
   * `analyze_results-aws.log`: a log file reporting information about the analysis of the results, including the parameters of the linear model fitted to the raw results.
   * `aws-scale.eps`: the plot of results in EPS format.
