# Installation of EasyCloud

## Prerequisites

* **Linux operating system.**
We suggest using [Fedora Linux](https://getfedora.org) version 33 or higher.

* **[Python](https://python.org) 3.7.7 (or greater) and [pip](https://pypi.org/project/pip/) 20.3.3 (or greater).**
Our experimental evaluation were performed by using _Python_ version 3.7.7, and _pip_ version 20.3.3 and version 21.1.3.
Since EasyCloud is entirely written in Python 3 language, it should be fully compatible with future versions of Python 3.
However, there could be some third-party packages used by EasyCloud (e.g., the ones uses to access a particular cloud platform) that exploit low-level features that may not work with future versions of Python 3.

* **[R](https://www.r-project.org/) 4.0.5 (or greater).**
The _R_ programming language is used to analyze results and generate plots.
In particular, results of our experimental evaluation were analyzed by using R 4.0.5.
However, higher version of R should work as well.

* **An archiver utility able to extract ZIP files.**
This utility will be used to extract some files compressed in ZIP format.
In this guide, we will assume that you will use the [UnZip](http://infozip.sourceforge.net/UnZip.html) utility, which is typically shipped with the most popular Linux distributions and that can be executed by running the command `unzip`.
 
* **Bash shell.**
Our shell script have been tested under the Bash shell (which is the default shell in Fedora Linux).
If you use another Linux distribution, make sure the `bash` command is installed in your system.

* **The Git revision control system.**
This utility will be used to clone this project on your computer.
Any version of Git should work; we used Git version 2.31.
 
* **A text editor.**
Any text editor can be used.
In this guide, we will assume that you will use the [Vim](https://www.vim.org/) text editor, a highly configurable text editor that is typically shipped with the most popular Linux distributions and that can be executed by running the command `vim`.


## Installation steps

In the following, we provide the steps needed to install EasyCloud in a Linux-like operating systems (and, in particular, on [Fedora Linux](https://getfedora.com)).
In the rest of this guide, we will denote with `EC_ARTIFACTS_HOME` the directory where you extracted the file bundle 

1. **Install the required software.**

    Open a terminal and run the following command:

	    sudo dnf install git python3 python3-pip R

	The following dependencies are optional and you can replace them with similar tools:

		sudo dnf install unzip

		sudo dnf install vim-enhanced

2. **Clone this project.**

        git clone https://gitlab.di.unipmn.it/sguazt/easycloud_artifacts-ccr2021.git

3. **Change to the directory where you cloned this project and set the environment variable `EC_ARTIFACTS_HOME` where to store this path.**

        cd easycloud_artifacts-ccr2021

        export EC_ARTIFACTS_HOME=$PWD

4. **Extract the EasyCloud source package.**

	    unzip easycloud-v2.0.2.zip

5. **Create a Python's virtual environment.**

    	python3 -m venv myenv

6. **Install the required core libraries.**

    	source myenv/bin/activate

        pip install -r easycloud/requirements.txt

		pip install numpy scipy

        deactivate
