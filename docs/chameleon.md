# Chameleon experiments

In this guide, we describe the [steps](#steps-for-reproducibility) required to reproduce the experiments on the [Chameleon Cloud](https://www.chameleoncloud.org/), which is an NSF-funded large-scale testbed system for Computer Science experimentation.
Chameleon is mainly based on an adapted and extended version of [OpenStack](https://www.openstack.org), which is one of the most popular open source cloud computing infrastructure software project.
Before performing any experiment, make sure to satisfy all the necessary [prerequisites](#prerequisites).


## Prerequisites

* **A Chameleon account.**
For these experiments, you need a Chameleon account; if you don't have one, you can create a free account in different ways (e.g., by using your Google credentials or even your existing organizational credentials if your institution is supported by Chameleon).
Please, visit the [Chalemeon home page](https://www.chameleoncloud.org/) and click on the "Log in" button.

* **A Chameleon project.**
In order to create Virtual Machine (VM) instances, you need to be a member of a Chameleon project.
If you are not member of any project, you can create a new one provided you are [PI eligible](https://chameleoncloud.readthedocs.io/en/latest/user/pi_eligibility.html).
The minimum resource requirements that a Chameleon project must meet to run the experimental evaluation are the following:
    * 45 instances
    * 45 VCPUs
    * 23 GB RAM
    * 47 network ports

* **The Chameleon CLI.**
To create VM instances instances, we provide a shell script that uses the Chameleon Command Line Interface (CLI).
Therefore, you need to install Chameleon CLI version and configure it properly, as described in the [Chameleon CLI page](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html).
We strongly suggest installing the Chameleon CLI so as you can automatically create the needed VM instance.
However, as an alternative to using our shell script, you can also create the needed VM instances manually by using the Web-based OpenStack dashboard of Chameleon.

* **The environment variable `EC_ARTIFACTS_HOME` is properly set.**
The environment variable `EC_ARTIFACTS_HOME` must contain the path to the directory where you cloned this project; this variable has been set during the [installation of EasyCloud](docs/install.md).


## Configuration settings

EasyCloud requires a configuration file specific to each cloud platform where to read information (such as the user credentials) needed to access and configure the services provided by a given cloud platform.
In the following, we report the configuration settings specific to Chameleon; settings highlighted in bold text are mandatory.

* **`os_auth_url`**: the identity authentication URL.
For example, `os_auth_url = https://kvm.tacc.chameleoncloud.org:5000`.

* **`os_username`**: the username associated with your Chameleon credentials.
For example, `os_username = foobar`.

* **`os_password`**: the password associated with your Chameleon credentials.
For example, `os_password = foobar_secret`.

* **`os_project_name`**: the name associated with your Chameleon project.
For example, `os_project_name = foobar_project`.

* **`os_project_id`**: the ID associated with your Chameleon project.
For example, `os_project_id = 0123456789abcdef0123456789abcdef`.

* **`os_region`**: the name of the default region to use when not specified.
For example, we used `os_region = KVM@TACC`.



## Steps for reproducibility

### Preliminary steps

The following steps are to be performed only the first time you access Chameleon with EasyCloud.

1. **Create a Chameleon account.**

   _NOTE: if you already have a Chameleon account, you can skip this step._

   To create a new Chameleon account, follow the instructions available in the [Chameleon documentation](https://chameleoncloud.readthedocs.io/en/latest/getting-started/index.html#step-1-log-in-to-chameleon).

2. **Create or join a project.**

   _NOTE: if you already are a member of project, you can use skip this step._

   To create a new project or join an existing one, follow the instructions available in the [Chameleon documentation](https://chameleoncloud.readthedocs.io/en/latest/getting-started/index.html#step-2-create-or-join-a-project).

3. <a name="create-a-key-pair">**Create a key pair.**</a>

   _NOTE: if you already have a key pair associated with your Chameleon project, you can use skip this step._

   To create a new key pair or import an existing one, follow the instructions available in the [Chameleon documentation](https://chameleoncloud.readthedocs.io/en/latest/technical/gui.html#key-pairs).

4. <a name="install-and-configura-chameleon-cli">**Install and configure Chameleon CLI.**</a>

    1. To install Chameleon CLI, follow the instructions available in the [Chameleon documentation](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html#installing-the-cli).
    2. To create authentication credentials to use with Chameleon CLI, follow the instructions available in the [Chameleon documentation](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html#cli-authentication).
      In particular, though you can either [_set a CLI password_](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html#setting-a-cli-password) or [_create an application credentials_](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html#creating-an-application-credential), we suggest [setting a CLI password](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html#setting-a-cli-password) as it is the easiest way.

5. <a name="download-the-openstack-rc-script">**Download the OpenStack RC script.**</a>

   The _OpenStack RC script_ is a shell script that contains the credentials required to access Chameleon features and it is used both by Chameleon CLI and EasyCloud.
   1. Download the OpenStack RC script by following the instructions available in the [Chameleon documentation](https://chameleoncloud.readthedocs.io/en/latest/technical/cli.html#the-openstack-rc-script).
   2. Edit the OpenStack RC script and replace the following lines (i.e., lines from 9 to 11):

          echo "($OS_USERNAME) Please enter your Chameleon CLI password: "
          read -sr OS_PASSWORD_INPUT
          export OS_PASSWORD=$OS_PASSWORD_INPUT

      with the following line:

          export OS_PASSWORD="<your-passowrd>"

      where `<your-password>` is the password you set with your authentication credentials (see [this step](#install-and-configure-chameleon-cli)).

6. **Create a virtual machine image.**

    Virtual machine instances will run the [Tiny Core Linux](http://tinycorelinux.net/) operating system version 11.1, which is a lightweight, highly modular Linux distribution.
    To this end, you first need to create a specific virtual machine image as follows:

        cd $EC_ARTIFACTS_HOME

        source myenv/bin/activate

		unzip resources/tinycorelinux-11.1-text.qcow2.zip -d resources

        ./scripts/chameleon_manage_instances.sh create-image resources/tinycorelinux-11.1-text.qcow2

        deactivate

6. **Create 45 instances.**

        cd $EC_ARTIFACTS_HOME

        source myenv/bin/activate

        ./scripts/chameleon_manage_instances.sh create <openstack-rc-script> 1 45 <key-name>

        deactivate

    where:
    * `<openstack-rc-script>`: the path to the OpenStack RC script that you downloaded before (see [this step](#download-the-openstack-rc-script));
    * `<key-name>`: the name of your key pair you created before (see [this step](#create-a-key-pair)).

5. **Create and edit the EasyCloud configuration file.**

	Before accessing Chameleon with EasyCloud the first time, you need to create a configuration file containing the security credentials of your Chameleon account and project.
    To do so, run the following commands:

        cd $EC_ARTIFACTS_HOME

        cd easycloud/modules/chameleon_openstacksdk

        cp settings.cfg.template settings.cfg

        vim settings.cfg

    Now, edit the configuration file `settings.cfg` to provide the settings associated with your Chameleon account and project (see [this section][#configuration-settings] for the list of settings specific to Chameleon).
    In particular, you can use the OpenStack RC script to provide the required settings as follows:
    * `os_auth_url`: the value of the `OS_AUTH_URL` variable in the OpenStack RC script. If the URL stored in the `OS_AUTH_URL` ends with `/v3`, the corresponding URL in `os_auth_url` must not contain that string. For instance, if `OS_AUTH_URL=https://kvm.tacc.chameleoncloud.org:5000/v3`, then `os_auth_url = https://kvm.tacc.chameleoncloud.org:5000`.

    * `os_username`: the value of the `OS_USERNAME` variable in the OpenStack RC script.

    * `os_password`: the value of the `OS_PASSWORD` variable in the OpenStack RC script.

    * `os_project_id`: the value of the `OS_PROJECT_ID` variable in the OpenStack RC script.

    * `os_region`: the value of the `OS_REGION` variable in the OpenStack RC script.


### Running experiments

1. **Run the experiments.**

        cd $EC_ARTIFACTS_HOME

        source myenv/bin/activate

        ./scripts/chameleon_easycloud.sh

        deactivate

   At the end of the execution, the `$EC_ARTIFACTS_HOME/logs/chameleon_easycloud` directory will contain the raw results.

2. **Analyze and plot results.**

        cd $EC_ARTIFACTS_HOME

        ./scripts/analyze_results.sh chameleon

   At the end of the execution, the `$EC_ARTIFACTS_HOME` directory will contain the following new files:
   * `chameleon.csv`: the raw results collected from the various log files generated by the experiments.
   * `analyze_results-chameleon.log`: a log file reporting information about the analysis of the results, including the parameters of the linear model fitted to the raw results.
   * `chameleon-scale.eps`: the plot of results in EPS format.

