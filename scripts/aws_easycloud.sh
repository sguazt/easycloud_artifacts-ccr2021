#!/usr/bin/env bash
# vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4:

###
### Run EasyCloud experiments on AWS.
###


ec_path="$EC_ARTIFACTS_HOME/easycloud"

export PYTHONPATH+=":$ec_path"

script=$(basename $0 .sh)
scriptdir=$(dirname $0)
pyscript=${scriptdir}/${script}.py
logsdir="$EC_ARTIFACTS_HOME/logs/$script"

ci_level=0.95
module_name=aws_awssdk
config_file=$ec_path/easycloud/modules/${module_name}/settings.cfg
metrics_file=$ec_path/rules/metrics.dct
rules_file=$ec_path/rules/rules.dct
instance_name_prefix='easycloud-'
min_num_runs=5
rel_prec=0.04
min_num_instances=5
max_num_instances=45
step_num_instances=5
zzz_time=5

# Increase the max number of open files
maxfile=$(ulimit -Sn)
ulimit -Sn unlimited

mkdir -p $logsdir

for ((num_instances=$min_num_instances; num_instances <= $max_num_instances; num_instances+=$step_num_instances )); do

	echo "Running benchmark for $num_instances instances..."

	bench_logfile=$logsdir/${script}-num_instances_${num_instances}-bench.log

	rm -f $bench_logfile

	sleep $zzz_time

	python ${pyscript} --ci-level $ci_level \
						--ec-config-file "$config_file" \
						--ec-metrics-file "$metrics_file" \
						--ec-rules-file "$rules_file" \
						--ec-module-name "$module_name" \
						--instance-name-prefix "$instance_name_prefix" \
						--min-num-runs $min_num_runs \
						--num-instances $num_instances \
						--rel-prec $rel_prec \
						--verbose \
						> $bench_logfile 2>&1

	sleep $zzz_time
done

# Restore old limit
ulimit -Sn $maxfile
