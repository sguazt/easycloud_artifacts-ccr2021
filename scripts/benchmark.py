import logging
import math
import numpy
import scipy.stats
import sys

import stopwatch
import time


class Benchmark:
    def __init__(self, ci_level=0.95, rel_prec=0.04, min_num_runs=5, max_num_runs=sys.maxsize):
        self._ci_level = ci_level
        self._rel_prec = rel_prec
        self._min_num_runs = min_num_runs
        self._max_num_runs = max_num_runs

    # Run a benchmark as follows:
    # 1. Calls the warm-up function `warmup_func`
    # 2. While stop conditions are not satisfied:
    # 3.  Calls the pre-run function `prerun_func`
    # 4.  Start the stopwatch
    # 5.  Calls the run function `run_func`
    # 6.  Stop the stopwatch
    # 7.  Calls the post-run function `postrun_func`
    # 8. Calls the cool-down function `cooldown_func`
    #
    def run(self, run_func, warmup_func=None, cooldown_func=None, prerun_func=None, postrun_func=None, **kwargs):
        if not callable(run_func):
            raise Exception("Function is not callable")

        # Run the warm-up function
        if warmup_func is not None and callable(warmup_func):
            warmup_func(**kwargs)

        cpu_time_stats = Statistics("CPU time")
        num_runs_estimator = CIRelativePrecisionNumRunsEstimator(self._ci_level, self._rel_prec, self._min_num_runs, self._max_num_runs)

        nreps = self._min_num_runs
        r = 0
        cpu_timings = []
        while r < nreps:
            logging.info("--- Run {} of {}]".format(r+1, nreps))

            # Run pre-run function
            if prerun_func is not None and callable(prerun_func):
                prerun_func(**kwargs)

            # Temporarily disable logging messages (except for warnings and errors)
            old_log_level = logging.getLogger().level
            logging.getLogger().setLevel(logging.WARNING)

            # Run the benchmark function
            timer = stopwatch.Stopwatch(timer=time.process_time)
            timer.start()
            run_func(**kwargs)
            timer.stop()

            # Restore original log level
            logging.getLogger().setLevel(old_log_level)

            # Collect statistics
            cpu_time_stats.collect(timer.duration)
            nreps = num_runs_estimator.estimate(cpu_time_stats)
            cpu_timings.append(timer.duration)

            if logging.getLogger().isEnabledFor(logging.INFO):
                logging.info("Increment benchmark stats")
                logging.info("* Run-level elapsed CPU time: {}".format(timer))
                logging.info("* Run-level incremental CPU time stats: {}".format(format_stats(cpu_time_stats, self._ci_level)))

            # Run post-run function
            if postrun_func is not None and callable(postrun_func):
                postrun_func(**kwargs)

            r += 1

        if logging.getLogger().isEnabledFor(logging.INFO):
            logging.info("Final benchmark stats")
            logging.info("* Collected CPU times: [{}]".format(','.join([str(x) for x in cpu_timings])))
            logging.info("* Final CPU time stats: {}".format(format_stats(cpu_time_stats, self._ci_level)))

        # Run the cool-down function
        if cooldown_func is not None and callable(cooldown_func):
            cooldown_func(**kwargs)

        return BenchmarkResult(cpu_time_stats, cpu_timings)


class ExternalTimeBenchmark(Benchmark):
    def run(self, run_func, elapsed_time_func, warmup_func=None, cooldown_func=None, prerun_func=None, postrun_func=None, **kwargs):
        if not callable(run_func):
            raise Exception("Function is not callable")

        # Run the warm-up function
        if warmup_func is not None and callable(warmup_func):
            warmup_func(**kwargs)

        cpu_time_stats = Statistics("CPU time")
        num_runs_estimator = CIRelativePrecisionNumRunsEstimator(self._ci_level, self._rel_prec, self._min_num_runs, self._max_num_runs)

        nreps = self._min_num_runs
        r = 0
        cpu_timings = []
        while r < nreps:
            logging.info("--- Run {} of {}]".format(r+1, nreps))

            # Run the pre-run function
            if prerun_func is not None and callable(prerun_func):
                prerun_func(**kwargs)

            # Temporarily disable logging messages (except for warnings and errors)
            old_log_level = logging.getLogger().level
            logging.getLogger().setLevel(logging.WARNING)

            # Run the benchmark function
            run_func(**kwargs)
            duration = elapsed_time_func()

            # Restore original log level
            logging.getLogger().setLevel(old_log_level)

            # Collect statistics
            cpu_time_stats.collect(duration)
            nreps = num_runs_estimator.estimate(cpu_time_stats)
            cpu_timings.append(duration)

            if logging.getLogger().isEnabledFor(logging.INFO):
                logging.info("Increment benchmark stats")
                logging.info("* Run-level elapsed CPU time: {}".format(duration))
                logging.info("* Run-level incremental CPU time stats: {}".format(format_stats(cpu_time_stats, self._ci_level)))

            # Run the post-run function
            if postrun_func is not None and callable(postrun_func):
                postrun_func(**kwargs)

            r += 1

        if logging.getLogger().isEnabledFor(logging.INFO):
            logging.info("Final benchmark stats")
            logging.info("* Collected CPU times: [{}]".format(','.join([str(x) for x in cpu_timings])))
            logging.info("* Final CPU time stats: {}".format(format_stats(cpu_time_stats, self._ci_level)))

        # Run the cool-down function
        if cooldown_func is not None and callable(cooldown_func):
            cooldown_func(**kwargs)

        return BenchmarkResult(cpu_time_stats, cpu_timings)


class BenchmarkResult:
    def __init__(self, cpu_time_stats, cpu_timings=[]):
        self.cpu_time_stats = cpu_time_stats
        self.cpu_timings = cpu_timings


class Statistics:
    DEFAULT_NAME = "Unnamed statistics"

    def __init__(self, name=DEFAULT_NAME):
        self._name = name
        self.clear()

    def clear(self):
        self._count = 0
        self._m1 = 0
        self._m2 = 0
        self._min = math.inf
        self._max = -math.inf

    def collect(self, value):
        self._count += 1
        self._max = max(self._max, value)
        self._min = min(self._min, value)

        # This algorithm comes from:
        #   D. Knuth, "The Art of Computer Programming, Volume II"
        # and is used to avoid round-off errors
        delta = value - self._m1
        self._m1 += delta/self._count # Uses the new value of _count!
        self._m2 += delta*(value-self._m1) # Uses the new value of _m1!

    def ci_mean(self, level):
        if self._count == 0:
            return (math.nan, math.nan, math.nan, math.nan)
        if self._count == 1:
            m = self.mean()
            return (m, m, 0, 1)

        m = self.mean()
        sd = self.standard_deviation()
        prob = (1-level)/2
        trv = scipy.stats.t(self._count-1)
        t = trv.ppf(1-prob)
        hw = t*sd/math.sqrt(self._count) # Half-width of the confidence interval
        rp = hw/math.fabs(m) if m != 0 else math.inf

        return (m-hw, m+hw, hw, rp)

    def count(self):
        return self._count

    def min(self):
        return self._min

    def max(self):
        return self._max

    def mean(self):
        return self._m1

    def name(self):
        return self._name

    def standard_deviation(self):
        return math.sqrt(self.variance())

    def sum(self):
        return self._m1*self._count

    def variance(self):
        if self._count == 0:
            return math.nan
        if self._count == 1:
            return 0
        # See: D. Knuth, "The Art of Computer Programming, Volume II"
        return self._m2/(self._count-1)

    def __str__(self):
        return "{} - mean: {}, s.d.: {}, min: {}, max: {}, count: {}".format(self.name(), self.mean(), self.standard_deviation(), self.min(), self.max(), self.count())


class CIRelativePrecisionNumRunsEstimator:
    DEFAULT_CI_LEVEL = 0.95
    DEFAULT_REL_PREC = 0.04
    DEFAULT_MIN_NUM_RUNS = 3
    DEFAULT_MAX_NUM_RUNS = sys.maxsize

    def __init__(self, ci_level=DEFAULT_CI_LEVEL, rel_prec=DEFAULT_REL_PREC, min_num_runs=DEFAULT_MIN_NUM_RUNS, max_num_runs=DEFAULT_MAX_NUM_RUNS):
        self._ci_level = ci_level
        self._target_rel_prec = rel_prec
        self._n_min = min_num_runs
        self._n_max = max_num_runs
        self._n_target = sys.maxsize
        self._aborted = False
        self._detected = False
        self._done = False
        self._initial_check = True

    def estimate(self, stats):
        n = stats.count()
        if self._done:
            return self._n_target
        if math.isinf(self._target_rel_prec):
            self._n_target = n
            self._detected = True
            self._done = True
        elif n < self._n_min:
            self._detected = False
            self._n_target = self._n_min
        elif n >= self._n_max:
            self._aborted = True
            self._n_target = self._n_max
        elif n <= 1:
            # NOTE: if n <= 1 we cannot use the procedure below (in the 'else' branch) as it uses a
            #       Student's t distribution with (n-1) degrees of freedom (in a Student's t distribution,
            #       the degrees of freedom must by greater than zero).
            self._detected = False
            self._n_target = self._n_max
        else:
            # Use the procedure described in [1], chapter 11.
            # However, instead of use an absolute precision (as done in [1]) we use
            # a relative precision with respect to the mean point estimator
            #
            # REFERENCES
            # 1. J. Banks, J.S. Carson II, B.L. Nelson and D.M. Nicol.
            #    "Discrete-event System Simulation," 4th Ed., Prentice-Hall, 2005
            # .
            #

            mean = stats.mean()
            sd = stats.standard_deviation()

            if sd <= 0 or math.isinf(sd):
                logging.warning("Standard deviation is negative or infinite")
                self._detected = False
            else:
                prob = (1+self._ci_level)*0.5
                n = stats.count()

                # Compute an initial estimate of sample size
                if self._initial_check:
                    self._initial_check = False

                    normrv = scipy.stats.norm()
                    z = normrv.ppf(prob)
                    n =  int(numpy.square(z*sd/(self._target_rel_prec*mean)))

                    if n < self._n_min:
                        n = self._n_min

                n_want = 0

                # Compute the real estimate of sample size
                while True:
                    trv = scipy.stats.t(n-1)
                    t = trv.ppf(prob)
                    n_want = numpy.square(t*sd/(self._target_rel_prec*mean))

                    if n < n_want:
                        n += 1
                    if n >= n_want or n > self._n_max:
                        break

                if n <= self._n_max:
                    if n <= stats.count():
                        self._done = True
                    self._n_target = n
                    self._detected = True
                else:
                    self._n_target = self._n_max
                    self._detected = False
                    self._aborted = True

        #logging.debug("({}) Detecting Sample Size --> {} (n_target_: {} - n_max_: {} - aborted_: {} - done: {})".format(stats.name(), self._detected, self._n_target, self._n_max, self._aborted, self._done))

        return self._n_target


def format_stats(stats, ci_level):
    ci = stats.ci_mean(ci_level)
    return "{} - mean: {}, s.d.: {}, ci@{}%: <[{}, {}], half-width: {}, relative-precision: {}>, min: {}, max: {}, count: {}".format(stats.name(), stats.mean(), stats.standard_deviation(), ci_level*100, ci[0], ci[1], ci[2], ci[3], stats.min(), stats.max(), stats.count())


def main():
    import time

    logging.basicConfig(level=logging.DEBUG)

    ci_level = 0.95
    rel_prec = 0.04
    min_num_runs = 5
    max_num_runs = sys.maxsize

    cpu_time_stats = Statistics("CPU time")
    num_runs_estimator = CIRelativePrecisionNumRunsEstimator(ci_level, rel_prec, min_num_runs, max_num_runs)

    nreps = min_num_runs
    r = 0
    while r < nreps:
        logging.debug("--- Run {}".format(r+1))

        start_time = time.process_time()
        fact = 2
        for i in range(3, 1000):
            fact *= i
        stop_time = time.process_time()

        cpu_time_stats.collect(stop_time-start_time)
        nreps = num_runs_estimator.estimate(cpu_time_stats)
        r += 1

        logging.debug(str(cpu_time_stats))

    print(format_stats(cpu_time_stats, ci_level))


if __name__ == '__main__':
    main()
