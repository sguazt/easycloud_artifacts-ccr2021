import argparse
import benchmark
from easycloud.core.compute import InstanceStatus
import easycloud.modules
import datetime
import importlib
import logging
import os
import sys
import time


class AWSBenchmarkUnit:
    def __init__(self, num_instances, config_file, rules_file, metrics_file, instance_name_prefix, enable_monitor, module_name):
        self._num_instances = num_instances
        self._config_file = config_file
        self._metrics_file = metrics_file
        self._module = module_name
        self._monitor = enable_monitor
        self._rules_file = rules_file
        self._mngr_cls = None
        self._mngr = None
        self._instance_name_prefix = instance_name_prefix
        # Import 'AWS module: easycloud.modules.<???>.manager' module (where <???> is self._module)
        # and retrieve the AWS class
        try:
            mod = importlib.import_module('.manager', '{}.{}'.format('easycloud.modules', self._module))
            self._mngr_cls = getattr(importlib.import_module('.manager', '{}.{}'.format('easycloud.modules', self._module)), 'AWS')
        except ImportError as err:
            logging.error("Unable to load module 'easycloud.modules.{}.manager': {}".format(self._module, err))
            raise err

    def warm_up(self):
        #logging.debug("WARM-UP: Using AWS with config file: {}, rules file: {}".format(self._config_file, self._rules_file))
        #self._mngr = AWS(self._config_file, self._rules_file)
        self._mngr = self._mngr_cls(self._config_file, self._rules_file)

    def cool_down(self):
        #logging.debug("COOL-DOWN: Stopping instances...")
        self._stop_instances()

    def pre_run(self):
        #logging.debug("PRE-RUN: Stopping instances...")
        self._stop_instances()
        if self._monitor:
            self._mngr.start_monitor(metrics_file=self._metrics_file)

    def post_run(self):
        #logging.debug("POST-RUN: Nothing to do.")
        if self._monitor:
            self._mngr.stop_monitor()

    def run(self):
        for instance in sorted(self._mngr.list_instances(), key=lambda inst: inst.name)[:self._num_instances]:
            if self._instance_name_prefix is None or instance.name.startswith(self._instance_name_prefix):
                logging.debug("Starting instance: {}".format(instance))
                instance.start()

    def _stop_instances(self):
        logging.debug("Stopping instances...")
        zzz_time = 5
        check = True
        while check:
            check = False
            #for instance in self._mngr.list_instances():
            for instance in sorted(self._mngr.list_instances(), key=lambda inst: inst.name)[:self._num_instances]:
                if self._instance_name_prefix is None or instance.name.startswith(self._instance_name_prefix):
                    # To stop an instance it is necessary that:
                    # - its status is not STOPPED and
                    # - there are no tasks (ongoing compute API calls) running over it.
                    # If a task is currently running, we wait for its termination (i.e., we repeatedly checks the
                    # 'task_state' property of a server until is becomes None)
                    if ('task_state' in instance.extra
                            and instance.extra['task_state'] is not None
                            and len(instance.extra['task_state']) > 0):
                        # Some task is running for this server (e.g., 'powering-on', 'powering-off', ...).
                        # Let's wait for its completion (i.e., sleep for few seconds and check again)
                        logging.debug("Waiting for task completion on instance: {}".format(instance))
                        time.sleep(zzz_time)
                        check = True
                    elif instance.state != InstanceStatus.STOPPED:
                        # This instance can be stopped because its status is not STOPPED and there are no running tasks
                        logging.debug("Stopping instance: {}".format(instance))
                        try:
                            instance.stop_node()
                        except Exception as err:
                            # If an instance takes a while to shut down, its state is still different than STOPPED but a call to stop() will throw an exception
                            logging.warning("Caught exception: {}".format(err))
                            time.sleep(zzz_time)
                        check = True


def main():
    easycloud_path = os.path.join(os.path.expanduser("~"), "Projects", "src", "easycloud")

    # Default value for CLI arguments
    default_ci_level = 0.95
    default_ec_config_file = os.path.join(easycloud_path, "easycloud", "modules", "aws_libcloud", "settings.cfg")
    default_ec_metrics_file = os.path.join(easycloud_path, "rules", "metrics.dct")
    default_ec_module_name = "aws_libcloud"
    default_ec_rules_file = os.path.join(easycloud_path, "rules", "rules.dct")
    default_instance_name_prefix = "easycloud-"
    default_min_num_runs = 5
    default_max_num_runs = sys.maxsize
    default_num_instances = 5
    default_rel_prec = 0.04

    # Parse CLI arguments
    parser = argparse.ArgumentParser(description="Run a benchmark for the AWS",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--ci-level', '-l',
                        type=float,
                        action='store',
                        help='Sets the level of the confidence interval',
                        default=default_ci_level)
    parser.add_argument('--debug', '-d',
                        action='store_true',
                        help='Shows debugging messages')
    parser.add_argument('--ec-config-file', '-c',
                        action='store',
                        help="Sets the path to the EasyCloud config file for the AWS module",
                        default=default_ec_config_file)
    parser.add_argument('--ec-metrics-file', '-t',
                        action='store',
                        help="Sets the path to the EasyCloud metrics file",
                        default=default_ec_metrics_file)
    parser.add_argument('--ec-module-name', '-u',
                        action='store',
                        help="Sets the module name to use for managing the AWS platform",
                        default=default_ec_module_name)
    parser.add_argument('--ec-monitor', '-s',
                        action='store_true',
                        help='Enables monitoring of VMs metrics')
    parser.add_argument('--ec-rules-file', '-r',
                        action='store',
                        help="Sets the path to the EasyCloud rules file",
                        default=default_ec_rules_file)
    parser.add_argument('--instance-name-prefix', '-P',
                        action='store',
                        help='Sets the prefix of the instance name',
                        default=default_instance_name_prefix)
    parser.add_argument('--max-num-runs', '-M',
                        type=int,
                        action='store',
                        help='Sets the maximum number of runs',
                        default=default_max_num_runs)
    parser.add_argument('--min-num-runs', '-m',
                        type=int,
                        action='store',
                        help='Sets the minimum number of runs',
                        default=default_min_num_runs)
    parser.add_argument('--num-instances', '-n',
                        type=int,
                        action='store',
                        help='Sets the maximum number of instances to start',
                        default=default_num_instances)
    parser.add_argument('--rel-prec', '-p',
                        type=float,
                        action='store',
                        help='Sets the wanted relative precision of the half-width confidence interval',
                        default=default_rel_prec)
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='Enables verbose output')
    args = parser.parse_args()

    print("RESTORE STUFF IN aws_easycloud.py")
    if args.debug:
        logging.basicConfig(level=logging.DEBUG,
                            #format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                            #datefmt='%Y-%m-%d:%H:%M:%S',
                            #format='[%(asctime)s (p%(process)s) {%(pathname)s:%(lineno)d}] %(levelname)s:%(message)s')
                            format='%(asctime)s %(levelname)-8s [(p%(process)s) %(pathname)s:%(lineno)d] %(message)s')
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)

    if args.verbose:
        logging.info("Options: {}".format(args))


    # Let's do the benchmark

    bench_driver = benchmark.Benchmark(ci_level = args.ci_level,
                                       rel_prec = args.rel_prec,
                                       min_num_runs = args.min_num_runs,
                                       max_num_runs = args.max_num_runs)

    aws_bench = AWSBenchmarkUnit(num_instances = args.num_instances,
                                 config_file = args.ec_config_file,
                                 enable_monitor = args.ec_monitor,
                                 instance_name_prefix = args.instance_name_prefix,
                                 metrics_file = args.ec_metrics_file,
                                 rules_file = args.ec_rules_file,
                                 module_name = args.ec_module_name)

    logging.info("BEGIN benchmark [{}]".format(datetime.datetime.now()))

    bench_res = bench_driver.run(run_func = aws_bench.run,
                                 warmup_func = aws_bench.warm_up,
                                 cooldown_func = aws_bench.cool_down,
                                 prerun_func = aws_bench.pre_run,
                                 postrun_func = aws_bench.post_run)

    logging.info("END benchmark [{}]".format(datetime.datetime.now()))

    print(bench_res.cpu_time_stats)


if __name__ == '__main__':
    main()
