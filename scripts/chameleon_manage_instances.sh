#!/usr/bin/env bash
# vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4:

##
## Manages virtual machine instances on the Chameleon Cloud testbed.
##


usage() {
	echo "Usage: $0 {create|create-image|delete|delete-image|help|list|net-list|start|stop} <openstack-rc-script> [options]"
	echo ""

	echo "Common options:"
	echo "- <openstack-rc-script>: The <openstack-rc-script> is the OpenStack RC Script needed to configure the environment variables for accessing Chameleon features. You can downloaded the script from the Chameleon GUI at the API Access."
	echo ""

	echo "ACTION 'create':"
	echo "  Usage: $0 create <openstack-rc-script> [<min>] [<max>] [<key-name>] [<prefix>]"
	echo "  Description: create (<max>+<min>+1) instances, where each instance is named as '<prefix>-<k>' (with <k>=<min>,...,<max>)."
	echo "  Options:"
	echo "  * <min>: an integer number representing the smallest number to associate with the first instance."
	echo "  * <max>: an integer number representing the largest number to associate with the last instance."
	echo "  * <key-name>: a string representing the key name of the key pair to inject to instances."
	echo "  * <prefix>: a string representing the prefix to use to name an instance."
	echo ""

	echo "ACTION 'create-image':"
	echo "  Usage: $0 create-image <openstack-rc-script> <image-file> <image-format> <image-name>"
	echo "  Description: create a new virtual machine image."
	echo "  Options:"
	echo "  * <image-file>: a string representing the name of the image file."
	echo "  * <image-format>: a string representing the image disk format. The supported formats are: ami, ari, aki, vhd, vmdk, raw, qcow2, vhdx, vdi, iso, ploop."
	echo "  * <image-name>: a string representing the image name."
	echo ""

	echo "ACTION 'delete':"
	echo "  Usage: $0 delete <openstack-rc-script> [<min>] [<max>] [<prefix>]"
	echo "  Description: delete instances in the [<min>, <max>] range."
	echo "  Options:"
	echo "  * <min>: an integer number representing the smallest number to associate with the first instance."
	echo "  * <max>: an integer number representing the largest number to associate with the last instance."
	echo "  * <prefix>: a string representing the prefix to use to name an instance."
	echo ""

	echo "ACTION 'delete-image':"
	echo "  Usage: $0 delete-image <openstack-rc-script> <image-name>"
	echo "  Description: delete an existing virtual machine image."
	echo "  Options:"
	echo "  * <image-name>: a string representing the image name."
	echo ""

	echo "ACTION 'help':"
	echo "  Usage: $0 help"
	echo "  Description: show this usage manual."
	echo ""

	echo "ACTION 'list':"
	echo "  Usage: $0 list <openstack-rc-script> [<min>] [<max>] [<prefix>]"
	echo "  Description: list instances in the [<min>, <max>] range."
	echo "  Options:"
	echo "  * <min>: an integer number representing the smallest number to associate with the first instance."
	echo "  * <max>: an integer number representing the largest number to associate with the last instance."
	echo "  * <prefix>: a string representing the prefix to use to name an instance."
	echo ""

	echo "ACTION 'net-list':"
	echo "  Usage: $0 net-list <openstack-rc-script>"
	echo "  Description: list networks."
	echo ""

	echo "ACTION 'start':"
	echo "  Usage: $0 start <openstack-rc-script> [<min>] [<max>] [<prefix>]"
	echo "  Description: start instances in the [<min>, <max>] range."
	echo "  Options:"
	echo "  * <min>: an integer number representing the smallest number to associate with the first instance."
	echo "  * <max>: an integer number representing the largest number to associate with the last instance."
	echo "  * <prefix>: a string representing the prefix to use to name an instance."
	echo ""

	echo "ACTION 'stop':"
	echo "  Usage: $0 stop <openstack-rc-script> [<min>] [<max>] [<prefix>]"
	echo "  Description: stop instances in the [<min>, <max>] range."
	echo "  Options:"
	echo "  * <min>: an integer number representing the smallest number to associate with the first instance."
	echo "  * <max>: an integer number representing the largest number to associate with the last instance."
	echo "  * <prefix>: a string representing the prefix to use to name an instance."
	echo ""
}


### main ###


if [ "$#" -lt 2 ]; then
	usage
	exit 1
fi

action=$1
openrc_script=$2

openrc_script=$(realpath $openrc_script)

. $openrc_script


# Default values
flavor="m1.tiny"
image="TinyCoreLinux-11.1-text"
imagefile="tinycorelinux-11.1-text.qcow2"
imagefmt="qcow2"
key="mykey"
max=45
min=1
network="sharednet1"
prefix="easycloud"
secgroup="default"
step=1
zone="nova"


case "$action" in
	create)

		if [ "$#" -gt 2 ]; then
			min=$3
		fi
		if [ "$#" -gt 3 ]; then
			max=$4
		fi
		if [ "$#" -gt 4 ]; then
			key=$5
		fi
		if [ "$#" -gt 5 ]; then
			prefix=$6
		fi

		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Creating instance '$name'..."
			openstack server create --image "$image" \
									--flavor "$flavor" \
									--key-name "$key" \
									--availability-zone "$zone" \
									--network "$network" \
									--security-group "$secgroup" \
									--wait \
									--quiet \
									"$name"
		done
		;;
	create-image)
		if [ "$#" -gt 2 ]; then
			imagefile=$3
		fi
		if [ "$#" -gt 3 ]; then
			imagefmt=$4
		fi
		if [ "$#" -gt 4 ]; then
			image=$5
		fi

		echo "Creating the virtual machine image '$image'..."
		openstack image create --disk-format "$imagefmt" \
								--container-format bare \
								--private \
								--file "$imagefile" \
								"$image"
		;;
	delete)
		if [ "$#" -gt 2 ]; then
			min=$3
		fi
		if [ "$#" -gt 3 ]; then
			max=$4
		fi
		if [ "$#" -gt 4 ]; then
			prefix=$5
		fi

		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Deleting instance '$name'..."
			openstack server delete $name
		done
		;;
	delete-image)
		if [ "$#" -gt 2 ]; then
			image=$3
		fi

		echo "Deleting the virtual machine image '$image'..."
		openstack image delete "$image"
		;;
	help)
		usage
		;;
	list)
		echo "Listing instances..."
		openstack server list
		;;
	net-list)
		echo "Listing networks..."
		openstack network list
		;;
	start)
		if [ "$#" -gt 2 ]; then
			min=$3
		fi
		if [ "$#" -gt 3 ]; then
			max=$4
		fi
		if [ "$#" -gt 4 ]; then
			prefix=$5
		fi

		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Starting instance '$name'..."
			openstack server start $name
		done
		;;
	stop)
		if [ "$#" -gt 2 ]; then
			min=$3
		fi
		if [ "$#" -gt 3 ]; then
			max=$4
		fi
		if [ "$#" -gt 4 ]; then
			prefix=$5
		fi

		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Stopping instance '$name'..."
			openstack server stop $name
		done
		;;
	*)
		echo "ERROR: Unknown action '$action'"
		exit 1
		;;
esac

