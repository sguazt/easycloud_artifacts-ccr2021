#!/usr/bin/env bash
# vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4:

###
### Analyze results from log files and generate plots.
###


usage() {
    echo "S0 <experiment-id> [<log-path>] [<output-format>]"
    echo "Options:"
    echo "- <experiment-id>: a string representing the representing the experiment to analyze."
    echo "  Possible values: aws, chameleon."
    echo "- <log-path>: a string representing the path to the folder containing the logs associated with <experiment-id>."
    echo "- <output-format>: a string representing the output graphics format of the plots."
    echo "  Possible values: eps, pdf, png."
}


# Path to the directory containing the scripts used to generated charts
scriptdir=$(dirname $0)
script=$(basename $0 .sh)

# Default values
errbar=ci
exp="chameleon"
logpath="$EC_ARTIFACTS_HOME/logs/${exp}_easycloud"
outfmt=eps

# Regular expressions
re1="-num_instances_([0-9]+)-"
re2=':\s*CPU time\s+-\s+mean:\s+([0-9]+.[0-9]+),\s+s\.d\.:\s+([0-9]+.[0-9]+),\s+ci@95%:\s+<\[([0-9]+.[0-9]+),\s+([0-9]+.[0-9]+)\]' #, count: ([0-9]+)$'

# Parse arguments
if [ $# -lt 1 ]; then
    usage
    exit 1
fi
if [ $# -gt 0 ]; then
    exp=$1
    logpath="$EC_ARTIFACTS_HOME/logs/${exp}_easycloud"
fi
if [ $# -gt 1 ]; then
    logpath=$2
fi
if [ $# -gt 2 ]; then
    outfmt=$3
fi

# Check arguments
if ! [ -e "$logpath" ]; then
    echo "ERROR: invalid log path '$logpath'."
    exit 1
fi

# Create a CSV file with results collected from logs
rm -f $csvfile
csvfile="$exp.csv"
echo '"sw","nvms","mean","sd","ci_lower","ci_upper"' > "$csvfile"
for f in $(ls $logpath); do
    logfile="$logpath/$f"
    if [[ "$logfile" =~ $re1 ]]; then
        nvms=${BASH_REMATCH[1]}
        stats=$(grep "Final CPU time stats" "$logfile")
        if [[ $stats =~ $re2 ]]; then
            echo "\"$exp\",$nvms,${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},${BASH_REMATCH[4]}" >> $csvfile
        fi
    fi
done

if ! [ -e $csvfile ]; then
    echo "ERROR: CSV file '$csvfile' not found!"
    exit 1
fi

logfile="$script-$exp.log"

Rscript --vanilla "$scriptdir/$script.R" --datfile "$csvfile" \
                                --errbar "$errbar" \
                                --outfmt "$outfmt" \
                                --verbose \
                                > "$logfile" 2>&1
ret=0

if [ $? == 0 ]; then
    echo "OK"
else
    echo "ERROR: something went wrong while analyzing results!"
    ret=1
fi

exit $ret
