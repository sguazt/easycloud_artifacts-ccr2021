#!/usr/bin/env bash
# vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4:

##
## Manage virtual machine instances on AWS.
##


usage() {
	echo "Usage: $0 {create|delete|list|start|stop} [min] [max] [key-name] [image] [instance-type]"
	echo "Options:"
	echo "- min, max: manage instances in the range between 'min' and 'max'."
	echo "- key-name: name of the AWS key."
	echo "- image: AMI identifier."
	echo "- instance-type: EC2 instance type."
}



if [ "$#" -lt 1 ]; then
	usage
	exit 1
fi

# Default values
image="ami-0dc2d3e4c0f9ebd18" # Amazon Linux 2 AMI (HVM), SSD Volume Type (64-bit x86)
instance_type="t2.micro"
key="mykey"
max=45
min=1
#network="vpc-29e12d54" # VPC default
#secgroup="default" # EC2-Classic default

action=$1
if [ "$#" -gt 1 ]; then
	min=$2
fi
if [ "$#" -gt 2 ]; then
	max=$3
fi
if [ "$#" -gt 3 ]; then
	key=$4
fi
if [ "$#" -gt 4 ]; then
	image=$5
fi
if [ "$#" -gt 5 ]; then
	instance_type=$6
fi

if ! command -v aws &> /dev/null ; then
	echo "ERROR: 'aws' command not found."
	exit 1
fi

prefix=easycloud
step=1

# NOTE: make sure to run the `aws configure` command to configure the AWS cli before using AWS

case "$action" in
	create)
		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Creating instance '$name'..."
			instance_id=$(\
					aws ec2 run-instances	--image-id "$image" \
											--count 1 \
											--instance-type "$instance_type" \
											--key-name "$key" \
											--enable-api-termination \
											--tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$name}]" \
											--no-paginate \
											--no-cli-pager \
											--output text --query 'Instances[*].InstanceId' \
											--no-dry-run \
				)
			if [ -n "$instance_id" ]; then
				aws ec2 wait instance-exists	--instance-ids $instance_id
				echo "Created instance '$name' -> '$instance_id'"
			else
				echo "Instance '$name' not created"
			fi
		done
		;;
	delete)
		echo "Deleting instances..."
		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Deleting instance '$name'..."
			instance_id=$(\
					aws ec2 describe-instances	--filters "Name=tag:Name,Values=$name" \
												--output text --query 'Reservations[*].Instances[*].InstanceId' \
												--no-dry-run \
				)
			if [ -n "$instance_id" ]; then
				aws ec2 terminate-instances	--instance-ids $instance_id \
											--no-dry-run
				echo "Deleted instance '$name' -> '$instance_id'"
			else
				echo "Instance '$name' not deleted"
			fi
		done
		;;
	list)
		aws ec2 describe-instances	--no-paginate \
									--no-cli-pager
		;;
	start)
		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Starting instance '$name'..."
			instance_id=$(\
					aws ec2 describe-instances	--filters "Name=tag:Name,Values=$name" \
												--output text --query 'Reservations[*].Instances[*].InstanceId' \
												--no-dry-run \
				)
			if [ -n "$instance_id" ]; then
				aws ec2 start-instances	--instance-ids $instance_id \
										--no-dry-run
				echo "Started instance '$name' -> '$instance_id'"
			else
				echo "Instance '$name' not started"
			fi
		done
		;;
	stop)
		for ((k = $min ; k <= $max ; k+=$step)); do
			name=$(printf "${prefix}-%03d" $k)

			echo "Stopping instance '$name'..."
			instance_id=$(\
					aws ec2 describe-instances	--filters "Name=tag:Name,Values=$name" \
												--output text --query 'Reservations[*].Instances[*].InstanceId' \
												--no-dry-run \
				)
			if [ -n "$instance_id" ]; then
				aws ec2 stop-instances	--instance-ids $instance_id \
										--no-dry-run
				echo "Stopped instance '$name' -> '$instance_id'"
			else
				echo "Instance '$name' not stopped"
			fi
		done
		;;
	*)
		echo "Unknown action '$action'"
		exit 1
		;;
esac
